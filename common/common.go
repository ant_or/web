package common

func Bound(vmin int, val int, vmax int) int {
	if val > vmax {
		val = vmax
	} else if val < vmin {
		val = vmin
	}
	return val
}
