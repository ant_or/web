package main

import (
	"encoding/json"
	"fmt"
	"passweb/http_server"
	"os"
)

func main() {

	type Config struct {
		Address  string `json:"address"`
	}
	var cng Config
	dat, err := os.ReadFile("passweb.ini")
	if err != nil {
		fmt.Println("Не найден passweb.ini: " + err.Error())
		os.Exit(1)
	}
	json.Unmarshal(dat, &cng)
	
	f, err := os.OpenFile("passweb.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Ошибка создания passweb.log: " + err.Error())
		os.Exit(1)
	}
	defer f.Close()

	os.Stdout = f
	os.Stderr = f

	http_server.Start(cng.Address, f)
	http_server.Wait()
}
