package user_context

import (
	"strings"
	"sync"
)

type UserContext struct {
	
}

var (
	userList map[string]UserContext = make(map[string]UserContext)
	mux      sync.Mutex
)

func GetUserContext(ipAddr string) UserContext {

	ipAddr = ipAddr[:strings.LastIndex(ipAddr, ":")]

	mux.Lock()
	user, ok := userList[ipAddr]
	mux.Unlock()
	if !ok {
		mux.Lock()
		userList[ipAddr] = UserContext{}
		user = userList[ipAddr]
		mux.Unlock()
	}
	return user
}
