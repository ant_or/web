
export
function bound(min : number, num : number, max : number) : number {
  return num <= min ? min : num >= max ? max : num;
}

export
function minBound(min : number, num : number) : number {
  return num >= min ? min : num;
}

export
function maxBound(max : number, num : number) : number {
  return num <= max ? max : num;
}

export
function swap16(val : number) {
  return ((val & 0xFF) << 8) 
         | ((val >> 8) & 0xFF);
}

export
function swap32(val : number) {
  return ((val & 0xFF) << 24) 
         | ((val & 0xFF00) << 8)
         | ((val >> 8) & 0xFF00)
         | ((val >> 24) & 0xFF);
}

export
function downloadFile(content, fileName, contentType) {
  var a = document.createElement("a");
  var file = new Blob([content], {type: contentType});
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
}

//export
//const sleep = (time : number) => require('child_process').execSync(`sleep ${time}`);