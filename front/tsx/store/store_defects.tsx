import {IDefect} from "../pass_types"

class DefectsStore{
    private m_defectsDB : Map<number, Array<IDefect>>;  // key weld id
    private m_defectsRec : Map<number, Array<IDefect>>;  // key weld id
    
    constructor() {        
        this.m_defectsDB = new Map<number, Array<IDefect>>();       
        this.m_defectsRec = new Map<number, Array<IDefect>>();        
    }
   
    getDefectsDB(wid : number){
        return this.m_defectsDB.has(wid) ? this.m_defectsDB.get(wid) : null;
    }
 
    setDefectsDB(wid : number, defs : Array<IDefect>){
        this.m_defectsDB.set(wid, defs);
    }

    getDefectsRec(wid : number){
        return this.m_defectsRec.has(wid) ? this.m_defectsRec.get(wid) : null;
    }
 
    setDefectsRec(wid : number, defs : Array<IDefect>){
        this.m_defectsRec.set(wid, defs);
    }

    clearDefectsRec(){
        this.m_defectsRec.clear();
    }
}

export
let Defects = new DefectsStore();

