import React, {FC} from "react";
import ReactDOM from 'react-dom/client';


import Grid from '@material-ui/core/Grid';
import StartScreen from "./start_screen/start_screen"

import "../css/style.css";

interface IProps {
};

const App: FC<IProps> = () => {
    return (
        <>
            <Grid container style={{height: "100%"}}>
                <Grid item xs>
                    <StartScreen/>
                </Grid>
            </Grid>
        </>
    )
}

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);

root.render(
    <App/>
);