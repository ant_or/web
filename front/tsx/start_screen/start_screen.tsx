import React from "react";

import Grid from '@material-ui/core/Grid';

import "../../css/style.css";
import logo from "../../images/logo.png";

interface IProps {
  
};
interface IState {
  
};

export default
class StartScreen extends React.Component<IProps, IState>{
    
  constructor(props : IProps){
    super(props);      

    this.state = {selSources : Array<number>()};
    
  }
  
  render(){   
    return(
      <Grid container style={{ height: "100%", width: "100vw"}}>   
        <Grid className="startScreenLeftPanel" item xs={2} >
          <img src={logo} />
        </Grid>
      </Grid>
    )
  }
}
