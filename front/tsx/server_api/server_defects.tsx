import { IDefect, IRecDefectParam } from "../pass_types";

export
function getDefects(srcId : number, weldId : Array<number>, onDefectsOfTube : (defect : Array<IDefect>) => any){

  let weldIdList = ""
  for (let i = 0; i < weldId.length; ++i){
    weldIdList += weldId[i];
    if (i <  weldId.length - 1)
      weldIdList += ",";
  }

  const req = 'api/v1/defects/' + srcId + '?weldIdList=' + weldIdList;
  fetch(req)
  .then(response => response.json())
  .then(onDefectsOfTube)
  .catch(() => console.log(req + ' error'));
}

export
function findDefects(srcId : number, weldId : Array<number>, recParams : IRecDefectParam,  onDefectsOfTube : (defect : Array<IDefect>) => any){

  let weldIdList = ""
  for (let i = 0; i < weldId.length; ++i){
    weldIdList += weldId[i];
    if (i <  weldId.length - 1)
      weldIdList += ",";
  }


  const req = 'api/v1/findDefects/' + srcId + '?weldIdList=' + weldIdList
                                            + '&sliceCoef=' + recParams.sliceCoef
                                            + '&phoneCoef=' + recParams.phoneCoef
                                            + '&widthMinMM=' + recParams.widthMinMM
                                            + '&widthMaxMM=' + recParams.widthMaxMM
                                            + '&heightMinMM=' + recParams.heightMinMM
                                            + '&heightMaxMM=' + recParams.heightMaxMM
                                            + '&weldIndentMM=' + recParams.weldIndentMM;
  fetch(req)
  .then(response => response.json())
  .then(onDefectsOfTube)
  .catch(() => console.log(req + ' error'));
}