/* eslint-disable no-undef */
const webpack = require('webpack');

module.exports = {
  entry: [
    'webpack-dev-server/client/?http://localhost:8080',
    './tsx/app.tsx',
  ],
  output: {
    publicPath: 'static/',
    path: __dirname + '/static/',
    filename: 'bundle.js'
  },
  devtool:  '#sourcemap',
  mode: 'development',
  resolve: {
    extensions: [".ts", ".tsx", '.css', ".js"],
    modules: ['node_modules']
  },
  module: {
    rules: [
      { test: /\.css$/, loader: 'style-loader!css-loader'},
      { test: /\.tsx?$/, exclude: /(node_modules)/, use: ['babel-loader'] },
      { test: /\.(jpe?g|png|gif|svg)$/i, loader: "url-loader?name=images/[name].[ext]"},
    ]
   },
  devServer: {
    hot: true,    
    proxy: {   
      '/api': 'http://localhost:8060',
    },   
  },  
  plugins: [new webpack.HotModuleReplacementPlugin()]   
}