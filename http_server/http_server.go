package http_server

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

var impl struct {
	server *http.Server
	router *chi.Mux
	done   chan bool
}

func Start(addr string, logout *os.File) {
	impl.router = chi.NewRouter()
	impl.server = &http.Server{Addr: addr, Handler: impl.router}
	impl.done = make(chan bool)

	//middleware.InitReq(logout)

	impl.router.Use(middleware.RequestID)
	impl.router.Use(middleware.RealIP)
	impl.router.Use(middleware.Logger)
	impl.router.Use(middleware.Recoverer)
	impl.router.Use(middleware.Timeout(60 * time.Second))

	go func() {
		log.Println("Запущен: " + addr)
		if err := impl.server.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("Ошибка старта http сервера: %v", err)
		}
		
		impl.done <- true
	}()

	createRoutes()
}

func Stop() {
	if err := impl.server.Shutdown(context.TODO()); err != nil {
		log.Fatalf("Ошибка остановки http сервера: %v", err)
	}
}

func Wait() {
	<-impl.done
}

func createRoutes() {

	impl.router.Handle("/*", http.FileServer(http.Dir("./front/static")))
	
}
