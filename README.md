# Сборка веб-приложения

1. Установить NodeJS  
https://nodejs.org/dist/v16.16.0/node-v16.16.0-linux-x64.tar.xz

2. Установить пакеты, выполнить в папке проекта /front:  
`npm install`

3. Собрать приложение, выполнить в папке проекта /front:  
`npm run build`


# Сборка сервера

1. Установить Go   
`wget -c https://go.dev/dl/go1.18.3.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local`  
В ~/.profile добавить  
`export PATH=$PATH:/usr/local/go/bin`  
Выполнить  
source ~/.profile

2. Собрать исп файл, выполнить в папке проекта:  
`go build`

# Запуск сервера

1. Запустить сервер, выполнить в папке проекта:  
`./passweb`

2. Зайти на страничку в браузере:  
`http://localhost:8060/`





